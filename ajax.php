<?php
session_start();
require 'conf.php';
require 'dbdriver.php';
require 'authdriver.php';

$db = new DBDriver($db_host, $db_user, $db_password, $db_db, $db_prefix, false);

if($_GET['action'] != 'auth' && (empty($_SESSION['auth']) || $_SESSION['auth'] !== true)){
	echo json_encode(false);
	exit();
}
switch($_GET['action']){

	case 'auth':
		if(isset($_SESSION['auth']) && $_SESSION['auth'] == true)
			echo json_encode(true);
		else
			echo json_encode(user_auth($_POST['username'],$_POST['password']));
	break;
	
	case 'disconnect':
		user_disconnect();
	break;
	
	case 'userlist':
		echo json_encode(user_search($_GET['pattern'].'%'));
	break;

	case 'dashboard':
		$tasks = array();
		$tasksResult = $db->query('SELECT t.id,t.name,t.detail,t.achived,t.priority,t.project FROM assigned a INNER JOIN '.$db->prefix.'tasks t ON a.task = t.id WHERE t.achived < 100 AND a.user = "'.$db->escape($_SESSION['username']).'" ORDER BY t.priority DESC, t.achived');
		
		while($task = $db->fetch_assoc($tasksResult)){
			
			$users = array();
			$parents = array();
			
			$parentResult = $db->query('SELECT p.parent, t.name FROM '.$db->prefix.'parents p INNER JOIN '.$db->prefix.'tasks t ON t.id = p.parent WHERE p.task = '.$task['id']);
			while($parent = $db->fetch_assoc($parentResult))
				$parents[] = json_encode(array($parent['parent'],$parent['name']));
				
			$userResult = $db->query('SELECT user FROM '.$db->prefix.'assigned WHERE task = '.$task['id']);
			while($user = $db->fetch_assoc($userResult))
				$users[] = $user['user'];
				
			$tasks[] = array(
				'id' => $task['id'],
				'project' => $task['project'],
				'name' => $task['name'],
				'detail' => $task['detail'],
				'achived' => $task['achived'],
				'priority' => $task['priority'],
				'parents' => $parents,
				'users' => $users
			);
		}
		echo json_encode($tasks);
	break;
	
	case 'tasknamelist':
	
		$res = $db->query('SELECT id,name FROM '.$db->prefix.'tasks WHERE id = "'.intval(urldecode($_GET['pattern'])).'" OR name LIKE "'.$db->escape(urldecode($_GET['pattern'])).'"');
		$tasks = array();
		while($task = $db->fetch_assoc($res)){
			$tasks[] = $task['name'].' #'.$task['id'];
		}
		echo json_encode($tasks);
		
	break;
	
	case 'addtask':
		
		if(!empty($_POST['name'])){
			
			$db->query('INSERT INTO '.$db->prefix.'tasks(name,detail,achived,project,priority) VALUES("'.$db->escape($_POST['name']).'","'.$db->escape($_POST['detail']).'",0,"'.$db->escape($_POST['project']).'","'.$db->escape($_POST['priority']).'")');
			$id = $db->insert_id();
			
			foreach($_POST['users'] as $user)
				$db->query('INSERT INTO '.$db->prefix.'assigned(user,task) VALUES("'.$db->escape($user).'",'.$id.')');
				
			foreach($_POST['tasks'] as $task)
				$db->query('INSERT INTO '.$db->prefix.'parents(parent,task) VALUES("'.$db->escape($task).'",'.$id.')');
		}
		print_r($db->saved_queries);
	break;
	
	case 'tasklist':

		$tasks = array();
		$tasksResult = $db->query('SELECT id,name,detail,achived,priority FROM '.$db->prefix.'tasks WHERE project = "'.$db->escape($_GET['project']).'" ORDER BY (achived = 100), priority DESC, achived');
		
		while($task = $db->fetch_assoc($tasksResult)){
			$users = array();
			$parents = array();
			
			$parentResult = $db->query('SELECT p.parent, t.name FROM '.$db->prefix.'parents p INNER JOIN '.$db->prefix.'tasks t ON t.id = p.parent WHERE p.task = '.$task['id']);
			while($parent = $db->fetch_assoc($parentResult))
				$parents[] = json_encode(array($parent['parent'],$parent['name']));
				
			$userResult = $db->query('SELECT user FROM '.$db->prefix.'assigned WHERE task = '.$task['id']);
			while($user = $db->fetch_assoc($userResult))
				$users[] = $user['user'];
				
			$tasks[] = array(
				'id' => $task['id'],
				'name' => $task['name'],
				'detail' => $task['detail'],
				'achived' => $task['achived'],
				'priority' => $task['priority'],
				'parents' => $parents,
				'users' => $users
			);
		}
		echo json_encode($tasks);
		
	break;
	
	case 'projectlist':
	
		$where = '';
	
		if(!empty($_GET['pattern']))
			$where = 'WHERE project LIKE "'.$db->escape(urldecode($_GET['pattern'])).'"';
	
		$projectsResult = $db->query('SELECT project FROM '.$db->prefix.'projects '.$where.' ORDER BY sorting');
		
		$projects = array();
		
		while($project = $db->fetch_assoc($projectsResult)){
			$projects[] = $project['project'];
		}
		
		echo json_encode($projects);
		
	break;
	
	case 'deletetask':
		$db->query('DELETE FROM '.$db->prefix.'tasks WHERE id = '.intval($_POST['task']));
	break;
	
	case 'deleteuser':
		$db->query('DELETE FROM '.$db->prefix.'assigned WHERE task = '.intval($_POST['task']).' AND user = "'.$db->escape($_POST['user']).'"');
	break;
	
	case 'adduser':
		$db->query('INSERT INTO '.$db->prefix.'assigned(task,user) VALUES('.intval($_POST['task']).',"'.$db->escape($_POST['user']).'")');
	break;
	
	case 'deleteparent':
		$db->query('DELETE FROM '.$db->prefix.'parents WHERE task = '.intval($_POST['task']).' AND parent = "'.intval($_POST['parent']).'"');
	break;
	
	case 'addparent':
		$db->query('INSERT INTO '.$db->prefix.'parents(task,parent) VALUES('.intval($_POST['task']).','.intval($_POST['parent']).')');
	break;
	
	case 'edittask':
		foreach(array('name','detail','achived','priority') as $prop)
			if(!empty($_POST[$prop]))
				$db->query('UPDATE '.$db->prefix.'tasks SET '.$prop.' = "'.$_POST[$prop].'" WHERE id = '.intval($_POST['task']));
				
		if(!empty($_POST['users'])){
			$db->query('DELETE FROM '.$db->prefix.'tasks WHERE id = '.intval($_POST['task']));
			foreach($_POST['users'] as $user)
				$db->query('INSERT INTO '.$db->prefix.'assigned(user,task) VALUES("'.$db->escape($user).'",'.$id.')');
		}
		
		if(!empty($_POST['parents'])){
			$db->query('DELETE FROM '.$db->prefix.'parents WHERE task = '.intval($_POST['task']));
			foreach($_POST['parents'] as $task)
				$db->query('INSERT INTO '.$db->prefix.'parents(parent,task) VALUES("'.$db->escape($task).'",'.$id.')');
		}
	break;
}
